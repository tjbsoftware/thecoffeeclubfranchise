﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.Mvc;
using CoffeeClubFranchisee.Models;
using System.Text;
using System.Net.Mail;
using System.Configuration;

namespace CoffeeClubFranchisee.Controllers
{
    public class ApplicationController : SurfaceController
    {
        [ChildActionOnly]
        public ActionResult FranchiseForm()
        {
            // In case you need it...
            //var currentNode = Umbraco.TypedContent(UmbracoContext.PageId.GetValueOrDefault());

            var model = new ApplicationForm();
            return PartialView("_ApplicationForm", model);
        }


        [HttpPost]
        public ActionResult FranchiseForm(ApplicationForm model)
        {
           // var errors = ModelState.Where(x => x.Value.Errors.Count > 0).Select(x => new { x.Key, x.Value.Errors }).ToArray();

            if (ModelState.IsValid)
            {
                var sb = new StringBuilder();
                sb.AppendFormat("<p><b>New Franchise Application:</b></p>");

                if (!string.IsNullOrEmpty(model.Name))
                {
                    sb.AppendFormat("<h2>Partner One</h2>");
                    sb.AppendFormat("<p><b>Name:</b> {0}</p>", model.Name);
                    sb.AppendFormat("<p><b>Email:</b> {0}</p>", model.Email);
                    sb.AppendFormat("<p><b>Phone:</b> {0}</p>", model.Phone);
                    sb.AppendFormat("<p><b>Mobile:</b> {0}</p>", model.Mobile);
                    sb.AppendFormat("<p><b>Fax:</b> {0}</p>", model.Fax);
                    sb.AppendFormat("<p><b>DOB:</b> {0}</p>", model.DOB);

                    //Address 
                    sb.AppendFormat("<h3>Address</h3>");
                    sb.AppendFormat("<p><b>Street Address:</b> {0}</p>", model.StreetNumber + " " + model.Address);
                    sb.AppendFormat("<p><b>Town/Suburb:</b> {0}</p>", model.Suburb);
                    sb.AppendFormat("<p><b>City/Region:</b> {0}</p>", model.City);
                    sb.AppendFormat("<p><b>Post Code:</b> {0}</p>", model.Postcode);
                    sb.AppendFormat("<p><b>Country:</b> {0}</p>", model.Country);
                    

                    
                    //Experiences
                    sb.AppendFormat("<h3>Previous Business /Employment Experience</h3>");
                    sb.Append("<table style='border:1px solid black'>");
                    sb.AppendFormat("<tr><td>Years</td><td>Position</td></tr>");
                    if(!string.IsNullOrEmpty(model.Years1)){
                         sb.AppendFormat("<tr><td>{0}</td><td>{1}</td></tr>",model.Years1,model.Position1 );
                    }
                    if(!string.IsNullOrEmpty(model.Years2))
                    {
                         sb.AppendFormat("<tr><td>{0}</td><td>{1}</td></tr>",model.Years2,model.Position2 );
                    }
                    if (!string.IsNullOrEmpty(model.Years3))
                    {
                        sb.AppendFormat("<tr><td>{0}</td><td>{1}</td></tr>", model.Years3, model.Position3);
                    }
                    if (!string.IsNullOrEmpty(model.Years4))
                    {
                        sb.AppendFormat("<tr><td>{0}</td><td>{1}</td></tr>", model.Years3, model.Position4);
                    }
                    if (!string.IsNullOrEmpty(model.Years5))
                    {
                        sb.AppendFormat("<tr><td>{0}</td><td>{1}</td></tr>", model.Years3, model.Position5);
                    }
                    sb.Append("</table>");

                    ///////////////////////////////////////////////////////////////////////////////////////////
                    sb.AppendFormat("<h3>Statement Of Assets and Liabilities</h3>");
                    sb.AppendFormat("<h4>Assets:</h4>");
                    sb.AppendFormat("<p><b>Bank:</b> {0}</p>", model.Bank);
                    sb.AppendFormat("<p><b>Business Investments:</b> {0}</p>",model.BusinessStatement);
                    sb.AppendFormat("<p><b>Real Estate:</b> {0}</p>", model.RealState);
                    sb.AppendFormat("<p><b>Share and Investments:</b> {0}</p>", model.ShareAndInvestment);
                    sb.AppendFormat("<p><b>Other:</b> {0}</p>", model.Other);

                    sb.AppendFormat("<h4>Liabilities:</h4>");
                    sb.AppendFormat("<p><b>Overdrafts:</b> {0}</p>", model.Overdrafts);
                    sb.AppendFormat("<p><b>Mortgages:</b> {0}</p>", model.Mortages);
                    sb.AppendFormat("<p><b>Credit Cards:</b> {0}</p>", model.CreditCards);
                    sb.AppendFormat("<p><b>Other:</b> {0}</p>", model.LiabilitiesOther);
                    sb.AppendFormat("<p><b>Total Liabilities:</b> {0}</p>", model.TotalLiabilities);
                    sb.AppendFormat("<p><b>Total Net Worth:</b> {0}</p>", model.TotalNetWorth);

                    sb.AppendFormat("<h3>Proposed Investment</h3>");
                    sb.AppendFormat("<p><b>Cash:</b> {0}</p>", model.Cash);
                    sb.AppendFormat("<p><b>Loan Type:</b> {0}</p>", model.LoanType);
                    sb.AppendFormat("<p><b>Total:</b> {0}</p>", model.Total);
                    sb.AppendFormat("<p><b>Preferred Location:</b> {0}</p>", model.PreferredLocation);
                    sb.AppendFormat("<p><b>Time Frame:</b> {0}</p>", model.Timeframe);
                    sb.AppendFormat("<br/>");

                    sb.AppendFormat("<p><b>Why have you chosen franchising?</b></p> <p> {0} </p>", model.WhyChosenFranchising);
                    sb.AppendFormat("<p><b>What is it about The Coffee Club that interests you over others in the industry?</b></p> <p> {0} </p>", model.WhatInterestsYouOverOthers);
                    sb.AppendFormat("<p><b>Tell us the motivating factors behind you wanting to own your own business?</b></p> <p> {0} </p>", model.MotivatingFactorsBehindOwn);
                    sb.AppendFormat("<p><b>What do you think will assist in making you a successful franchisee with The Coffee Club?</b></p> <p> {0} </p>", model.AssistMakingSuccessfulFranchisee);
                    sb.AppendFormat("<p><b>Where did you first find out about afranchising opportunity with The Coffee Club?</b></p> <p> {0} </p>", model.FindAboutFranchisingOpportunity);

                }

                if (!string.IsNullOrEmpty(model.Name2))
                {
                    sb.AppendFormat("<h2>Partner Two</h2>");
                    sb.AppendFormat("<p><b>Name:</b> {0}</p>", model.Name2);
                    sb.AppendFormat("<p><b>Fax:</b> {0}</p>", model.Email2);
                    sb.AppendFormat("<p><b>Phone:</b> {0}</p>", model.Phone2);
                    sb.AppendFormat("<p><b>Mobile:</b> {0}</p>", model.Mobile2);
                    sb.AppendFormat("<p><b>Fax:</b> {0}</p>", model.Fax2);
                    sb.AppendFormat("<p><b>DOB:</b> {0}</p>", model.DOB2);

                    //Address 
                    sb.AppendFormat("<h3>Address</h3>");
                    sb.AppendFormat("<p><b>Street Address:</b> {0}</p>", model.StreetNumber2 + " " + model.Address2);
                    sb.AppendFormat("<p><b>Town/Suburb:</b> {0}</p>", model.Suburb2);
                    sb.AppendFormat("<p><b>City/Region:</b> {0}</p>", model.City2);
                    sb.AppendFormat("<p><b>Post Code:</b> {0}</p>", model.Postcode2);
                    sb.AppendFormat("<p><b>Country:</b> {0}</p>", model.Country2);



                    //Experiences
                    sb.AppendFormat("<h3>Previous Business /Employment Experience</h3>");
                    sb.Append("<table style='border:1px solid black'>");
                    sb.AppendFormat("<tr><td>Years</td><td>Position</td></tr>");
                    if (!string.IsNullOrEmpty(model.Years12))
                    {
                        sb.AppendFormat("<tr><td>{0}</td><td>{1}</td></tr>", model.Years12, model.Position12);
                    }
                    if (!string.IsNullOrEmpty(model.Years22))
                    {
                        sb.AppendFormat("<tr><td>{0}</td><td>{1}</td></tr>", model.Years22, model.Position22);
                    }
                    if (!string.IsNullOrEmpty(model.Years32))
                    {
                        sb.AppendFormat("<tr><td>{0}</td><td>{1}</td></tr>", model.Years32, model.Position32);
                    }
                    if (!string.IsNullOrEmpty(model.Years42))
                    {
                        sb.AppendFormat("<tr><td>{0}</td><td>{1}</td></tr>", model.Years32, model.Position42);
                    }
                    if (!string.IsNullOrEmpty(model.Years52))
                    {
                        sb.AppendFormat("<tr><td>{0}</td><td>{1}</td></tr>", model.Years32, model.Position52);
                    }
                    sb.Append("</table>");

                    ///////////////////////////////////////////////////////////////////////////////////////////
                    sb.AppendFormat("<h3>Statement Of Assets and Liabilities</h3>");
                    sb.AppendFormat("<h4>Assets:</h4>");
                    sb.AppendFormat("<p><b>Bank:</b> {0}</p>", model.Bank2);
                    sb.AppendFormat("<p><b>Business Investments:</b> {0}</p>", model.BusinessStatement2);
                    sb.AppendFormat("<p><b>Real Estate:</b> {0}</p>", model.RealState2);
                    sb.AppendFormat("<p><b>Share and Investments:</b> {0}</p>", model.ShareAndInvestment2);
                    sb.AppendFormat("<p><b>Other:</b> {0}</p>", model.Other2);

                    sb.AppendFormat("<h4>Liabilities:</h4>");
                    sb.AppendFormat("<p><b>Overdrafts:</b> {0}</p>", model.Overdrafts2);
                    sb.AppendFormat("<p><b>Mortgages:</b> {0}</p>", model.Mortages2);
                    sb.AppendFormat("<p><b>Credit Cards:</b> {0}</p>", model.CreditCards2);
                    sb.AppendFormat("<p><b>Other:</b> {0}</p>", model.LiabilitiesOther2);
                    sb.AppendFormat("<p><b>Total Liabilities:</b> {0}</p>", model.TotalLiabilities2);
                    sb.AppendFormat("<p><b>Total Net Worth:</b> {0}</p>", model.TotalNetWorth2);

                    sb.AppendFormat("<h3>Proposed Investment</h3>");
                    sb.AppendFormat("<p><b>Cash:</b> {0}</p>", model.Cash2);
                    sb.AppendFormat("<p><b>Loan Type:</b> {0}</p>", model.LoanType2);
                    sb.AppendFormat("<p><b>Total:</b> {0}</p>", model.Total2);
                    sb.AppendFormat("<p><b>Preferred Location:</b> {0}</p>", model.PreferredLocation2);
                    sb.AppendFormat("<p><b>Time Frame:</b> {0}</p>", model.Timeframe2);
                    sb.AppendFormat("<br/>");

                    sb.AppendFormat("<p><b>Why have you chosen franchising?</b></p> <p> {0} </p>", model.WhyChosenFranchising2);
                    sb.AppendFormat("<p><b>What is it about The Coffee Club that interests you over others in the industry?</b></p> <p> {0} </p>", model.WhatInterestsYouOverOthers2);
                    sb.AppendFormat("<p><b>Tell us the motivating factors behind you wanting to own your own business?</b></p> <p> {0} </p>", model.MotivatingFactorsBehindOwn2);
                    sb.AppendFormat("<p><b>What do you think will assist in making you a successful franchisee with The Coffee Club?</b></p> <p> {0} </p>", model.AssistMakingSuccessfulFranchisee2);
                    sb.AppendFormat("<p><b>Where did you first find out about afranchising opportunity with The Coffee Club?</b></p> <p> {0} </p>", model.FindAboutFranchisingOpportunity2);

                }




                var Sender = model.Email;
                MailMessage mailMessage = new MailMessage(Sender, "aashish.bhurtel@yahoo.com");
                mailMessage.IsBodyHtml = true;
                mailMessage.Body = sb.ToString();
                mailMessage.Subject = "Franchise Application Request";
                SmtpClient smtpClient = new SmtpClient();
                smtpClient.Send(mailMessage);

                TempData["Success"] = "Thanks for your application request.";
                return RedirectToUmbracoPage(model.RedirectPage);
            }
            return this.RedirectToCurrentUmbracoPage();
        }
    }
}