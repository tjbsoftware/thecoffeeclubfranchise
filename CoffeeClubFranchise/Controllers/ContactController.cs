﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.Mvc;
using CoffeeClubFranchisee.Models; 
using System.Text;
using System.Net.Mail;
using System.Configuration;

namespace CoffeeClubFranchisee.Controllers
{
    public class ContactController : SurfaceController
    {
        [ChildActionOnly]
        public ActionResult ContactForm()
        {
            // In case you need it...
            //var currentNode = Umbraco.TypedContent(UmbracoContext.PageId.GetValueOrDefault());

            var model = new ContactModel();
            return PartialView("ContactForm", model);
        }


        [HttpPost]
        public ActionResult ContactForm(ContactModel model)
        {
            if (ModelState.IsValid)
            {
                var sb = new StringBuilder();
                sb.AppendFormat("<p><b>Name:</b> {0}</p>", model.Name);
                sb.AppendFormat("<p><b>Email:</b> {0}</p>", model.Email);
                sb.AppendFormat("<p><b>Phone:</b> {0}</p>", model.Phone);
                sb.AppendFormat("<p><b>Subject:</b> {0}</p>", model.Subject);
                sb.AppendFormat("<p><b>Message:</b> {0}</p>", model.Message);

                var Sender = model.Email;
                MailMessage mailMessage = new MailMessage(Sender, "aashish.bhurtel@yahoo.com");
                mailMessage.IsBodyHtml = true;
                mailMessage.Body = sb.ToString();
                mailMessage.Subject = model.Subject;
                SmtpClient smtpClient = new SmtpClient();
                smtpClient.Send(mailMessage);




                TempData["Success"] = "Thanks for your message.";
                return RedirectToUmbracoPage(model.RedirectPage);
            }
            return this.RedirectToCurrentUmbracoPage();
        }

    }
}
