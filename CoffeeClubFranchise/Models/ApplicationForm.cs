﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CoffeeClubFranchisee.Models
{
    public class ApplicationForm
    {
        public int RedirectPage { get; set; }

        [Required]
        public string Name { get; set; }
  
        [Display(Name = "Street Address ")]
        [StringLength(10)]
        public string StreetNumber { get; set; }


        [Display(Name = "Street Address")]
        [StringLength(45)]
        public string Address { get; set; }

     
        [Display(Name = "Town/Suburb")]
        [StringLength(45)]
        public string Suburb { get; set; }

        [Display(Name = "City/Region")]
        [StringLength(45)]
        public string City { get; set; }

      
        [Display(Name = "Post Code")]
        [StringLength(20)]
        public string Postcode { get; set; }

 
        public string Country { get; set; }

        public string Phone { get; set; }

        public string Mobile { get; set; }

        public string Fax { get; set; }

        public DateTime DOB{ get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        //Experiences
        public string Years1 { get; set; }

        public string Position1 { get; set; }

        public string Years2 { get; set; }

        public string Position2 { get; set; }

        public string Years3 { get; set; }

        public string Position3 { get; set; }

        public string Years4 { get; set; }

        public string Position4 { get; set; }

        public string Years5 { get; set; }

        public string Position5 { get; set; }

        //STATEMENT OFASSETS & LIABILITIES

        public string Bank { get; set; }
        [Display(Name = "Business Statement")]
        public string BusinessStatement { get; set; }
        [Display(Name = "Real State")]
        public string RealState { get; set; }
         [Display(Name = "Share And Investment")]

        public string ShareAndInvestment { get; set; }
        public string Other { get; set; }

        //Liabilities
        public string Overdrafts { get; set; }

        [Display(Name = "Mortgages")]
        public string Mortages { get; set; }

         [Display(Name = "Credit Cards")]
        public string CreditCards { get; set; }

         [Display(Name = "Liabilities Other")]
        public string LiabilitiesOther { get; set; }

         [Display(Name = "Total Liabilities ")]
        public string TotalLiabilities { get; set; }

         [Display(Name = "Total Net Worth")]
        public string TotalNetWorth{ get; set; }

        //PROPOSED INVESTMENT
        public string Cash{ get; set; }
        [Display(Name = "Loan Type")]
        public string LoanType{ get; set; }
        public string Total{ get; set; }
        [Display(Name = "Preferred Location")]
        public string PreferredLocation{ get; set; }
        [Display(Name = "Time frame")]
        public string Timeframe{ get; set; }

        //Questions
        [Display(Name = "Why have you chosen franchising?")]
        public string WhyChosenFranchising { get; set; }
        [Display(Name = "What is it about The Coffee Club that interests you over others in the industry?")]
        public string WhatInterestsYouOverOthers { get; set; }

         [Display(Name = "Tell us the motivating factors behind you wanting to own your own business?")]
        public string MotivatingFactorsBehindOwn { get; set; }

        [Display(Name = "What do you think will assist in making you a successful franchisee with The Coffee Club?")]
        public string AssistMakingSuccessfulFranchisee { get; set; }

        [Display(Name = "Where did you first find out about a franchising opportunity with The Coffee Club?")]
        public string FindAboutFranchisingOpportunity { get; set; }

        /// <summary>
        /// //////////////////////////////////For Second Partner
        /// </summary>


        [Display(Name = "Name")]
        public string Name2 { get; set; }

        [Display(Name = "Street Address ")]
        [StringLength(10)]
        public string StreetNumber2 { get; set; }


        [Display(Name = "Street Address")]
        [StringLength(45)]
        public string Address2 { get; set; }


        [Display(Name = "Town/Suburb")]
        [StringLength(45)]
        public string Suburb2 { get; set; }

        [Display(Name = "City/Region")]
        [StringLength(45)]
        public string City2 { get; set; }


        [Display(Name = "Post Code")]
        [StringLength(20)]
        public string Postcode2 { get; set; }

        [Display(Name = "Country")] 
        public string Country2 { get; set; }

        [Display(Name = "Phone")]
        public string Phone2 { get; set; }

        [Display(Name = "Mobile")]
        public string Mobile2 { get; set; }

        [Display(Name = "Fax")]
        public string Fax2 { get; set; }

        [Display(Name = "DOB")]
        public DateTime DOB2 { get; set; }

        [Display(Name = "Email")]
        public string Email2 { get; set; }

        //Experiences
        public string Years12 { get; set; }

        public string Position12 { get; set; }

        public string Years22 { get; set; }

        public string Position22 { get; set; }

        public string Years32 { get; set; }

        public string Position32 { get; set; }

        public string Years42 { get; set; }

        public string Position42 { get; set; }

        public string Years52 { get; set; }

        public string Position52 { get; set; }

        //STATEMENT OFASSETS & LIABILITIES
        [Display(Name = "Bank")]
        public string Bank2 { get; set; }
        [Display(Name = "Business Statement")]
        public string BusinessStatement2 { get; set; }
        [Display(Name = "Real State")]
        public string RealState2 { get; set; }

        [Display(Name = "Share And Investment")]
        public string ShareAndInvestment2 { get; set; }
         [Display(Name = "Other")]
        public string Other2 { get; set; }

        //Liabilities
        [Display(Name = "Overdrafts")]
        public string Overdrafts2 { get; set; }
        [Display(Name = "Mortgages")]
        public string Mortages2 { get; set; }

        [Display(Name = "Credit Cards")]
        public string CreditCards2 { get; set; }

        [Display(Name = "Liabilities Other")]
        public string LiabilitiesOther2 { get; set; }

        [Display(Name = "Total Liabilities ")]
        public string TotalLiabilities2 { get; set; }

        [Display(Name = "Total Net Worth")]
        public string TotalNetWorth2 { get; set; }

        //PROPOSED INVESTMENT
        public string Cash2 { get; set; }
        [Display(Name = "Loan Type")]
        public string LoanType2 { get; set; }
        public string Total2 { get; set; }
        [Display(Name = "Preferred Location")]
        public string PreferredLocation2 { get; set; }
        [Display(Name = "Time frame")]
        public string Timeframe2 { get; set; }

        //Questions
        [Display(Name = "Why have you chosen franchising?")]
        public string WhyChosenFranchising2 { get; set; }
        [Display(Name = "What is it about The Coffee Club that interests you over others in the industry?")]
        public string WhatInterestsYouOverOthers2 { get; set; }

        [Display(Name = "Tell us the motivating factors behind you wanting to own your own business?")]
        public string MotivatingFactorsBehindOwn2 { get; set; }

        [Display(Name = "What do you think will assist in making you a successful franchisee with The Coffee Club?")]
        public string AssistMakingSuccessfulFranchisee2 { get; set; }

        [Display(Name = "Where did you first find out about a franchising opportunity with The Coffee Club?")]
        public string FindAboutFranchisingOpportunity2 { get; set; }   





    }
}